export const categoriesMock: any = {
  _id: '5d1f9ced016c3c2d40a1b7ad',
  region: 'FR',
  src: 'Alkemics',
  children: [
    {
      name: 'Hygiène & cosmétologie',
      link: {
        uri: '/r/hygiène---cosmétologie',
      },
      children: [
        {
          name: 'Cosmeto-textile',
          code: '70059',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70059/hygiène---cosmétologie/cosmeto-textile',
          },
        },
        {
          name: 'Accessoire de maquillage',
          code: '70067',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70067/hygiène---cosmétologie/accessoire-de-maquillage',
          },
        },
        {
          name: 'Accessoire de soin',
          code: '70057',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70057/hygiène---cosmétologie/accessoire-de-soin',
          },
        },
        {
          name: 'Accessoire pour cheveux',
          code: '70052',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70052/hygiène---cosmétologie/accessoire-pour-cheveux',
          },
        },
        {
          name: "Autre accessoire d'hygiène bucco-dentaire",
          code: '101001',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101001/hygiène---cosmétologie/autre-accessoire-d-hygiène-bucco-dentaire',
          },
        },
        {
          name: 'Bain de bouche',
          code: '101000',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101000/hygiène---cosmétologie/bain-de-bouche',
          },
        },
        {
          name: 'Bain-douche & savon',
          code: '100811',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100811/hygiène---cosmétologie/bain-douche---savon',
          },
        },
        {
          name: 'Brosse à dents',
          code: '100998',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100998/hygiène---cosmétologie/brosse-à-dents',
          },
        },
        {
          name: 'Coloration',
          code: '70051',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70051/hygiène---cosmétologie/coloration',
          },
        },
        {
          name: 'Démaquillant et nettoyant',
          code: '70054',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70054/hygiène---cosmétologie/démaquillant-et-nettoyant',
          },
        },
        {
          name: 'Déodorant & anti-transpirant',
          code: '100812',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100812/hygiène---cosmétologie/déodorant---anti-transpirant',
          },
        },
        {
          name: 'Douche et bain',
          code: '100993',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100993/hygiène---cosmétologie/douche-et-bain',
          },
        },
        {
          name: 'Hygiène',
          code: '80747',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/80747/hygiène---cosmétologie/hygiène',
          },
        },
        {
          name: 'Hygiène & cosmétologie',
          code: '80728',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/80728/hygiène---cosmétologie/hygiène---cosmétologie',
          },
        },
        {
          name: 'Dentifrice',
          code: '100999',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100999/hygiène---cosmétologie/dentifrice',
          },
        },
        {
          name: 'Hygiène dentaire',
          code: '100814',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100814/hygiène---cosmétologie/hygiène-dentaire',
          },
        },
        {
          name: 'Hygiène intime',
          code: '100815',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100815/hygiène---cosmétologie/hygiène-intime',
          },
        },
        {
          name: 'Maquillage',
          code: '70060',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70060/hygiène---cosmétologie/maquillage',
          },
        },
        {
          name: 'Maquillage - Lèvres',
          code: '70063',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70063/hygiène---cosmétologie/maquillage---lèvres',
          },
        },
        {
          name: 'Maquillage - Visage',
          code: '70061',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70061/hygiène---cosmétologie/maquillage---visage',
          },
        },
        {
          name: 'Maquillage - Yeux',
          code: '70062',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70062/hygiène---cosmétologie/maquillage---yeux',
          },
        },
        {
          name: 'Mouchoir, coton, coton-tige',
          code: '70047',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70047/hygiène---cosmétologie/mouchoir--coton--coton-tige',
          },
        },
        {
          name: 'Parfum',
          code: '70068',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70068/hygiène---cosmétologie/parfum',
          },
        },
        {
          name: "Produit d'épilation",
          code: '101006',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101006/hygiène---cosmétologie/produit-d-épilation',
          },
        },
        {
          name: 'Produit de rasage',
          code: '101005',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101005/hygiène---cosmétologie/produit-de-rasage',
          },
        },
        {
          name: 'Produit solaire',
          code: '70058',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70058/hygiène---cosmétologie/produit-solaire',
          },
        },
        {
          name: 'Protection hygiénique',
          code: '101002',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101002/hygiène---cosmétologie/protection-hygiénique',
          },
        },
        {
          name: 'Rasage et épilation',
          code: '100810',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100810/hygiène---cosmétologie/rasage-et-épilation',
          },
        },
        {
          name: 'Rasoirs et lames',
          code: '101004',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101004/hygiène---cosmétologie/rasoirs-et-lames',
          },
        },
        {
          name: 'Savon et gel main',
          code: '100992',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100992/hygiène---cosmétologie/savon-et-gel-main',
          },
        },
        {
          name: 'Shampooing et soin',
          code: '70049',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70049/hygiène---cosmétologie/shampooing-et-soin',
          },
        },
        {
          name: 'Soin cheveux',
          code: '70048',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70048/hygiène---cosmétologie/soin-cheveux',
          },
        },
        {
          name: 'Coiffant gel et laque',
          code: '70050',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70050/hygiène---cosmétologie/coiffant-gel-et-laque',
          },
        },
        {
          name: 'Soin corps (incluant pieds et mains)',
          code: '70056',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70056/hygiène---cosmétologie/soin-corps--incluant-pieds-et-mains-',
          },
        },
        {
          name: 'Soin visage',
          code: '70055',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70055/hygiène---cosmétologie/soin-visage',
          },
        },
        {
          name: 'Soin visage & corps',
          code: '70053',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70053/hygiène---cosmétologie/soin-visage---corps',
          },
        },
        {
          name: 'Toilette intime',
          code: '101003',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101003/hygiène---cosmétologie/toilette-intime',
          },
        },
        {
          name: 'Vernis, dissolvant',
          code: '71551',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71551/hygiène---cosmétologie/vernis--dissolvant',
          },
        },
      ],
    },
    {
      name: 'Santé',
      link: {
        uri: '/r/santé',
      },
      children: [
        {
          name: 'Baby care',
          code: '70015',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70015/santé/baby-care',
          },
        },
        {
          name: 'Make-up - Body',
          code: '70066',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70066/santé/make-up---body',
          },
        },
        {
          name: 'Accessoire',
          code: '100819',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/100819/santé/accessoire',
          },
        },
        {
          name: 'Aromathérapie',
          code: '71547',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71547/santé/aromathérapie',
          },
        },
        {
          name: 'Auditif, nasal',
          code: '101007',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101007/santé/auditif--nasal',
          },
        },
        {
          name: 'Bandage, pansement, sparadrap',
          code: '101010',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101010/santé/bandage--pansement--sparadrap',
          },
        },
        {
          name: 'Désinfectant, antiseptique',
          code: '101011',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101011/santé/désinfectant--antiseptique',
          },
        },
        {
          name: 'Douleur articulaire, dos et tête',
          code: '71648',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71648/santé/douleur-articulaire--dos-et-tête',
          },
        },
        {
          name: 'Extrait de plante standardisé (EPS)',
          code: '71545',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71545/santé/extrait-de-plante-standardisé--eps-',
          },
        },
        {
          name: 'Incontinence',
          code: '71543',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71543/santé/incontinence',
          },
        },
        {
          name: 'Instrument de mesure et test',
          code: '101008',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101008/santé/instrument-de-mesure-et-test',
          },
        },
        {
          name: 'Lunettes et loupe',
          code: '71538',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71538/santé/lunettes-et-loupe',
          },
        },
        {
          name: 'Matériel médical',
          code: '71548',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71548/santé/matériel-médical',
          },
        },
        {
          name: 'Orthopédie',
          code: '71550',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71550/santé/orthopédie',
          },
        },
        {
          name: 'Petits maux quotidiens',
          code: '70069',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70069/santé/petits-maux-quotidiens',
          },
        },
        {
          name: 'Premiers soins',
          code: '101009',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/101009/santé/premiers-soins',
          },
        },
        {
          name: 'Préservatif et produit intime',
          code: '80746',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/80746/santé/préservatif-et-produit-intime',
          },
        },
        {
          name: 'Santé',
          code: '71537',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71537/santé/santé',
          },
        },
        {
          name: 'Trouble digestif',
          code: '72218',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/72218/santé/trouble-digestif',
          },
        },
        {
          name: 'Santé naturelle',
          code: '71544',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71544/santé/santé-naturelle',
          },
        },
        {
          name: 'Traitement gorge/nez/oreille',
          code: '70070',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70070/santé/traitement-gorge/nez/oreille',
          },
        },
        {
          name: 'Vêtement de contention',
          code: '71549',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71549/santé/vêtement-de-contention',
          },
        },
      ],
    },
    {
      name: 'Aliment',
      link: {
        uri: '/r/aliment',
      },
      children: [
        {
          name: 'Sweet wine, Porto, bitters',
          code: '100978',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100978/aliment/sweet-wine--porto--bitters',
          },
        },
        {
          name: 'Aniseed',
          code: '100977',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100977/aliment/aniseed',
          },
        },
        {
          name: 'Vodka, gin, tequila',
          code: '70027',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70027/aliment/vodka--gin--tequila',
          },
        },
        {
          name: 'Whisky & Bourbon',
          code: '100980',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100980/aliment/whisky---bourbon',
          },
        },
        {
          name: 'Rum',
          code: '100979',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100979/aliment/rum',
          },
        },
        {
          name: 'Savoury spread',
          code: '100759',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100759/aliment/savoury-spread',
          },
        },
        {
          name: 'Breaded and coated fish',
          code: '70019',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70019/aliment/breaded-and-coated-fish',
          },
        },
        {
          name: 'Aide culinaire',
          code: '100758',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100758/aliment/aide-culinaire',
          },
        },
        {
          name: 'Aide culinaire & préparation pour pâtisserie',
          code: '100773',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100773/aliment/aide-culinaire---préparation-pour-pâtisserie',
          },
        },
        {
          name: 'Aliment',
          code: '80727',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/80727/aliment/aliment',
          },
        },
        {
          name: 'Œuf',
          code: '82930',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82930/aliment/œuf',
          },
        },
        {
          name: 'Biscuit salé et chips',
          code: '100982',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100982/aliment/biscuit-salé-et-chips',
          },
        },
        {
          name: 'Beurre',
          code: '100795',
          nutriscore: 'grasa',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100795/aliment/beurre',
          },
        },
        {
          name: 'Bière',
          code: '82916',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82916/aliment/bière',
          },
        },
        {
          name: 'Biscuit',
          code: '100764',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100764/aliment/biscuit',
          },
        },
        {
          name: 'Boisson',
          code: '82913',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82913/aliment/boisson',
          },
        },
        {
          name: 'Boisson alcoolisée',
          code: '82914',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82914/aliment/boisson-alcoolisée',
          },
        },
        {
          name: 'Boisson chocolatée à préparer',
          code: '82922',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82922/aliment/boisson-chocolatée-à-préparer',
          },
        },
        {
          name: 'Soda & autre BSA prête-à-boire',
          code: '82920',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82920/aliment/soda---autre-bsa-prête-à-boire',
          },
        },
        {
          name: 'Boisson lactée prête-à-boire',
          code: '70004',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70004/aliment/boisson-lactée-prête-à-boire',
          },
        },
        {
          name: 'Sirop et autre préparation pour boisson aromatisée',
          code: '82921',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82921/aliment/sirop-et-autre-préparation-pour-boisson-aromatisée',
          },
        },
        {
          name: 'Boisson non alcoolisée',
          code: '82918',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82918/aliment/boisson-non-alcoolisée',
          },
        },
        {
          name: 'Apéritif, cocktail, bière et vin sans alcool',
          code: '100755',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100755/aliment/apéritif--cocktail--bière-et-vin-sans-alcool',
          },
        },
        {
          name: 'Bonbon, dragée, sucette...',
          code: '100767',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100767/aliment/bonbon--dragée--sucette---',
          },
        },
        {
          name: 'Bouillon et assaisonnement',
          code: '100984',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100984/aliment/bouillon-et-assaisonnement',
          },
        },
        {
          name: 'Café & chicorée',
          code: '82923',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82923/aliment/café---chicorée',
          },
        },
        {
          name: 'Céréales (non transformé)',
          code: '100800',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100800/aliment/céréales--non-transformé-',
          },
        },
        {
          name: 'Céréales (transformé/préparé)',
          code: '82940',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82940/aliment/céréales--transformé/préparé-',
          },
        },
        {
          name: 'Céréales petit-déjeuner & barre de céréale',
          code: '100770',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100770/aliment/céréales-petit-déjeuner---barre-de-céréale',
          },
        },
        {
          name: 'Charcuterie',
          code: '82927',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82927/aliment/charcuterie',
          },
        },
        {
          name: 'Chewing-gum',
          code: '100768',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100768/aliment/chewing-gum',
          },
        },
        {
          name: 'Confiserie chocolatée',
          code: '100766',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100766/aliment/confiserie-chocolatée',
          },
        },
        {
          name: 'Cidre & poiré',
          code: '100753',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100753/aliment/cidre---poiré',
          },
        },
        {
          name: 'Complément alimentaire',
          code: '100817',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100817/aliment/complément-alimentaire',
          },
        },
        {
          name: 'Sauce froide et condimentaire',
          code: '100808',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100808/aliment/sauce-froide-et-condimentaire',
          },
        },
        {
          name: 'Confiserie',
          code: '100765',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100765/aliment/confiserie',
          },
        },
        {
          name: 'Confiture',
          code: '100775',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100775/aliment/confiture',
          },
        },
        {
          name: 'Cornichon, oignon, câpre, olive (saumure)',
          code: '100805',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100805/aliment/cornichon--oignon--câpre--olive--saumure-',
          },
        },
        {
          name: 'Digestif & Liqueur',
          code: '70026',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70026/aliment/digestif---liqueur',
          },
        },
        {
          name: 'Eau',
          code: '82919',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82919/aliment/eau',
          },
        },
        {
          name: 'Épicerie salée',
          code: '100757',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100757/aliment/épicerie-salée',
          },
        },
        {
          name: 'Terrine, pâté, foie gras, rillettes',
          code: '100781',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100781/aliment/terrine--pâté--foie-gras--rillettes',
          },
        },
        {
          name: 'Sauce chaude',
          code: '100762',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100762/aliment/sauce-chaude',
          },
        },
        {
          name: 'Épicerie sucrée',
          code: '100763',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100763/aliment/épicerie-sucrée',
          },
        },
        {
          name: 'Escargot, grenouille...',
          code: '100784',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100784/aliment/escargot--grenouille---',
          },
        },
        {
          name: 'Farine',
          code: '70022',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70022/aliment/farine',
          },
        },
        {
          name: 'Frites, purées et autres préparations à base de pomme de terre et autres légumes',
          code: '82946',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82946/aliment/frites--purées-et-autres-préparations-à-base-de-pomme-de-terre-et-autres-légumes',
          },
        },
        {
          name: 'Fruit (non transformé)',
          code: '100801',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100801/aliment/fruit--non-transformé-',
          },
        },
        {
          name: 'Fruit, fruit sec, graines oléagineuses (transformé/préparé)',
          code: '100797',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100797/aliment/fruit--fruit-sec--graines-oléagineuses--transformé/préparé-',
          },
        },
        {
          name: 'Fruits de mer',
          code: '100782',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100782/aliment/fruits-de-mer',
          },
        },
        {
          name: 'Fruits, Céréales & Légumes (transformés/préparés)',
          code: '82938',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82938/aliment/fruits--céréales---légumes--transformés/préparés-',
          },
        },
        {
          name: 'Fruits, Céréales & Légumes bruts conditionnés',
          code: '100799',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100799/aliment/fruits--céréales---légumes-bruts-conditionnés',
          },
        },
        {
          name: 'Gibier',
          code: '100790',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100790/aliment/gibier',
          },
        },
        {
          name: 'Glace & Sorbet',
          code: '100771',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100771/aliment/glace---sorbet',
          },
        },
        {
          name: 'Jus de fruits et légumes',
          code: '100756',
          nutriscore: 'bebida',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100756/aliment/jus-de-fruits-et-légumes',
          },
        },
        {
          name: 'Kit pour plat',
          code: '82947',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82947/aliment/kit-pour-plat',
          },
        },
        {
          name: 'Lait végétal, crème végétale',
          code: '82943',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82943/aliment/lait-végétal--crème-végétale',
          },
        },
        {
          name: 'Légume & champignon (non transformé)',
          code: '100803',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100803/aliment/légume---champignon--non-transformé-',
          },
        },
        {
          name: 'Légume & champignon (transformé/préparé)',
          code: '82939',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82939/aliment/légume---champignon--transformé/préparé-',
          },
        },
        {
          name: 'Légume sec / légumineuse, plante (non transformé)',
          code: '100802',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100802/aliment/légume-sec-/-légumineuse--plante--non-transformé-',
          },
        },
        {
          name: 'Légume sec / légumineuses (transformé/préparé)',
          code: '100798',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100798/aliment/légume-sec-/-légumineuses--transformé/préparé-',
          },
        },
        {
          name: 'Matière grasse animale',
          code: '82931',
          nutriscore: 'grasa',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82931/aliment/matière-grasse-animale',
          },
        },
        {
          name: 'Matière grasse végétale',
          code: '82941',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82941/aliment/matière-grasse-végétale',
          },
        },
        {
          name: 'Miel',
          code: '100776',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100776/aliment/miel',
          },
        },
        {
          name: 'Moutarde',
          code: '100806',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100806/aliment/moutarde',
          },
        },
        {
          name: 'Pâte à tartiner',
          code: '100777',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100777/aliment/pâte-à-tartiner',
          },
        },
        {
          name: 'Pâtes alimentaires',
          code: '100761',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100761/aliment/pâtes-alimentaires',
          },
        },
        {
          name: 'Plat cuisiné',
          code: '82948',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82948/aliment/plat-cuisiné',
          },
        },
        {
          name: 'Plat cuisiné à base de viande, poisson, céréales, soja, pâtes, riz, blé',
          code: '70007',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70007/aliment/plat-cuisiné-à-base-de-viande--poisson--céréales--soja--pâtes--riz--blé',
          },
        },
        {
          name: 'Viande préparée',
          code: '70020',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70020/aliment/viande-préparée',
          },
        },
        {
          name: 'Plat préparé',
          code: '82945',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82945/aliment/plat-préparé',
          },
        },
        {
          name: 'Poisson cru',
          code: '70012',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70012/aliment/poisson-cru',
          },
        },
        {
          name: 'Poivre, sel, épice, herbes...',
          code: '100807',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100807/aliment/poivre--sel--épice--herbes---',
          },
        },
        {
          name: 'Prêt à manger – Sandwich',
          code: '70005',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70005/aliment/prêt-à-manger-–-sandwich',
          },
        },
        {
          name: 'Produit animal',
          code: '82925',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82925/aliment/produit-animal',
          },
        },
        {
          name: 'Fromage',
          code: '100793',
          nutriscore: 'queso',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100793/aliment/fromage',
          },
        },
        {
          name: 'Produit laitier & dessert frais lacté',
          code: '100791',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100791/aliment/produit-laitier---dessert-frais-lacté',
          },
        },
        {
          name: 'Lait nature',
          code: '100794',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100794/aliment/lait-nature',
          },
        },
        {
          name: 'Produit panifié',
          code: '100804',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100804/aliment/produit-panifié',
          },
        },
        {
          name: 'Produits de la mer ou des rivières',
          code: '82928',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82928/aliment/produits-de-la-mer-ou-des-rivières',
          },
        },
        {
          name: 'Traiteur de la mer',
          code: '82929',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82929/aliment/traiteur-de-la-mer',
          },
        },
        {
          name: 'Punch, Cocktail & Boisson prémix',
          code: '70025',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70025/aliment/punch--cocktail---boisson-prémix',
          },
        },
        {
          name: 'Quiche, tarte, pizza, crêpe, burger',
          code: '100989',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100989/aliment/quiche--tarte--pizza--crêpe--burger',
          },
        },
        {
          name: 'Soupe',
          code: '82952',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82952/aliment/soupe',
          },
        },
        {
          name: 'Spiritueux',
          code: '100754',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100754/aliment/spiritueux',
          },
        },
        {
          name: 'Sucre & Édulcorant',
          code: '70023',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70023/aliment/sucre---édulcorant',
          },
        },
        {
          name: 'Thé & infusion',
          code: '82924',
          nutriscore: 'no',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82924/aliment/thé---infusion',
          },
        },
        {
          name: 'Viande bovine',
          code: '100786',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100786/aliment/viande-bovine',
          },
        },
        {
          name: 'Viande chevaline',
          code: '100785',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100785/aliment/viande-chevaline',
          },
        },
        {
          name: 'Viande fraîche',
          code: '82926',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82926/aliment/viande-fraîche',
          },
        },
        {
          name: 'Viande porcine',
          code: '100788',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100788/aliment/viande-porcine',
          },
        },
        {
          name: 'Viennoiserie & pâtisserie',
          code: '100778',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100778/aliment/viennoiserie---pâtisserie',
          },
        },
        {
          name: 'Vin & champagne, mousseux',
          code: '82915',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82915/aliment/vin---champagne--mousseux',
          },
        },
        {
          name: 'Vinaigre',
          code: '100809',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100809/aliment/vinaigre',
          },
        },
        {
          name: 'Volaille',
          code: '100789',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100789/aliment/volaille',
          },
        },
        {
          name: 'Yaourt, dessert végétal',
          code: '70008',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/70008/aliment/yaourt--dessert-végétal',
          },
        },
      ],
    },
    {
      name: 'Animalerie',
      link: {
        uri: '/r/animalerie',
      },
      children: [
        {
          name: 'Accessoire pour animal',
          code: '82906',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/82906/animalerie/accessoire-pour-animal',
          },
        },
        {
          name: 'Aliment pour animal',
          code: '80734',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/80734/animalerie/aliment-pour-animal',
          },
        },
        {
          name: 'Litière et hygiène pour animal',
          code: '82905',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/82905/animalerie/litière-et-hygiène-pour-animal',
          },
        },
        {
          name: 'Animalerie',
          code: '82904',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/82904/animalerie/animalerie',
          },
        },
        {
          name: 'Soin vétérinaire',
          code: '72219',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/72219/animalerie/soin-vétérinaire',
          },
        },
      ],
    },
    {
      name: 'Maison',
      link: {
        uri: '/r/maison',
      },
      children: [
        {
          name: 'Maison',
          code: '82956',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/82956/maison/maison',
          },
        },
      ],
    },
    {
      name: 'Matériel électrique',
      link: {
        uri: '/r/matériel-électrique',
      },
      children: [
        {
          name: 'Matériel électrique',
          code: '82972',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/82972/matériel-électrique/matériel-électrique',
          },
        },
      ],
    },
    {
      name: "Produit d'entretien ménager et du quotidien",
      link: {
        uri: '/r/produit-d-entretien-ménager-et-du-quotidien',
      },
      children: [
        {
          name: 'Acaricide, insecticide, raticide...',
          code: '71568',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/71568/produit-d-entretien-ménager-et-du-quotidien/acaricide--insecticide--raticide---',
          },
        },
        {
          name: 'Accessoire chaussure',
          code: '101100',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101100/produit-d-entretien-ménager-et-du-quotidien/accessoire-chaussure',
          },
        },
        {
          name: 'Accessoire de nettoyage',
          code: '82968',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/82968/produit-d-entretien-ménager-et-du-quotidien/accessoire-de-nettoyage',
          },
        },
        {
          name: 'Assouplissant',
          code: '101299',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101299/produit-d-entretien-ménager-et-du-quotidien/assouplissant',
          },
        },
        {
          name: 'Auxiliaire de lavage',
          code: '101300',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101300/produit-d-entretien-ménager-et-du-quotidien/auxiliaire-de-lavage',
          },
        },
        {
          name: 'Bricolage',
          code: '71487',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/71487/produit-d-entretien-ménager-et-du-quotidien/bricolage',
          },
        },
        {
          name: 'Cirage et entretien chaussures',
          code: '101099',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101099/produit-d-entretien-ménager-et-du-quotidien/cirage-et-entretien-chaussures',
          },
        },
        {
          name: 'Désodorisant',
          code: '82969',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/82969/produit-d-entretien-ménager-et-du-quotidien/désodorisant',
          },
        },
        {
          name: "Entretien d'appareil électroménager",
          code: '100821',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/100821/produit-d-entretien-ménager-et-du-quotidien/entretien-d-appareil-électroménager',
          },
        },
        {
          name: 'Essuie tout & papier toilette',
          code: '100822',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/100822/produit-d-entretien-ménager-et-du-quotidien/essuie-tout---papier-toilette',
          },
        },
        {
          name: 'Lessive',
          code: '101298',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101298/produit-d-entretien-ménager-et-du-quotidien/lessive',
          },
        },
        {
          name: 'Lessive & soin du linge',
          code: '82965',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/82965/produit-d-entretien-ménager-et-du-quotidien/lessive---soin-du-linge',
          },
        },
        {
          name: 'Liquide vaisselle main',
          code: '101075',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101075/produit-d-entretien-ménager-et-du-quotidien/liquide-vaisselle-main',
          },
        },
        {
          name: 'Nettoyant ménager',
          code: '82962',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/82962/produit-d-entretien-ménager-et-du-quotidien/nettoyant-ménager',
          },
        },
        {
          name: "Produit d'entretien ménager et du quotidien",
          code: '80729',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/80729/produit-d-entretien-ménager-et-du-quotidien/produit-d-entretien-ménager-et-du-quotidien',
          },
        },
        {
          name: 'Produit lave-vaisselle',
          code: '101076',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101076/produit-d-entretien-ménager-et-du-quotidien/produit-lave-vaisselle',
          },
        },
        {
          name: 'Produits complémentaires de soin du linge',
          code: '101301',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101301/produit-d-entretien-ménager-et-du-quotidien/produits-complémentaires-de-soin-du-linge',
          },
        },
        {
          name: 'Repoussant, piège',
          code: '71569',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/71569/produit-d-entretien-ménager-et-du-quotidien/repoussant--piège',
          },
        },
        {
          name: 'Sac poubelle',
          code: '101296',
          nutriscore: 'no',
          main_attr: 'l-efficate',
          link: {
            uri: '/r/101296/produit-d-entretien-ménager-et-du-quotidien/sac-poubelle',
          },
        },
      ],
    },
    {
      name: 'Puériculture',
      link: {
        uri: '/r/puériculture',
      },
      children: [
        {
          name: 'Aliment pour bébé',
          code: '100975',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100975/puériculture/aliment-pour-bébé',
          },
        },
        {
          name: 'Aliment pour bébé diversification',
          code: '82909',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82909/puériculture/aliment-pour-bébé-diversification',
          },
        },
        {
          name: 'Boisson infantile',
          code: '100824',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100824/puériculture/boisson-infantile',
          },
        },
        {
          name: 'Change pour bébé',
          code: '70014',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70014/puériculture/change-pour-bébé',
          },
        },
        {
          name: 'Hygiène bébé',
          code: '70017',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70017/puériculture/hygiène-bébé',
          },
        },
        {
          name: 'Hygiène et soin puériculture',
          code: '82911',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/82911/puériculture/hygiène-et-soin-puériculture',
          },
        },
        {
          name: 'Lait infantile',
          code: '82910',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/82910/puériculture/lait-infantile',
          },
        },
        {
          name: 'Petit accessoire de puériculture',
          code: '70018',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70018/puériculture/petit-accessoire-de-puériculture',
          },
        },
        {
          name: 'Plat et dessert infantile',
          code: '100825',
          nutriscore: 'ok',
          main_attr: 'le-grout',
          link: {
            uri: '/r/100825/puériculture/plat-et-dessert-infantile',
          },
        },
        {
          name: 'Puériculture',
          code: '82908',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/82908/puériculture/puériculture',
          },
        },
      ],
    },
    {
      name: 'Tabac',
      link: {
        uri: '/r/tabac',
      },
      children: [
        {
          name: 'Tabac',
          code: '72201',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/72201/tabac/tabac',
          },
        },
      ],
    },
    {
      name: 'Textile',
      link: {
        uri: '/r/textile',
      },
      children: [
        {
          name: 'Textile',
          code: '70071',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70071/textile/textile',
          },
        },
      ],
    },
    {
      name: 'Bricolage',
      link: {
        uri: '/r/bricolage',
      },
      children: [
        {
          name: 'Bricolage',
          code: '71487',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/71487/bricolage/bricolage',
          },
        },
      ],
    },
    {
      name: 'Jardinerie',
      link: {
        uri: '/r/jardinerie',
      },
      children: [
        {
          name: 'Jardinerie',
          code: '70073',
          nutriscore: 'no',
          main_attr: 'la-qualite',
          link: {
            uri: '/r/70073/jardinerie/jardinerie',
          },
        },
      ],
    },
  ],
};
