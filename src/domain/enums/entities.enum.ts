export enum Entities {
  Author = 'Author',
  Book = 'Book',
  Product = 'Product',
}

export enum Timestamps {
  UpdatedAt = 'updated',
  CreatedAt = 'date',
}
