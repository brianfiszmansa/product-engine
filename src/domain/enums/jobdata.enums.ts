export enum JobdataStatus {
  ACTIVE = 'active',
  FINISHED = 'finished',
}
