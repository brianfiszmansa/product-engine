export const SRServiceName = 'SchemaRegistryService';

export const enum SchemaTypes {
  RECORD = 'record',
}

export const enum SchemaNames {
  PRODUCTS = 'product_schema',
}

export const enum SchemaNamespaces {
  PRODUCTS = 'product.schema.avro',
}
