export interface Errors {
  date: Date;
  location: string;
  producer: string;
  message: string;
  error: string | undefined;
}
